<?php
      # Delete Motivo
      if(@$_GET['acao'] && $_GET['acao'] == 'delete_motivo'):
        $tabela = new ocorrencias;
        $id = $_GET['id'];
        $tabela->tabela_1 = "tipo_ocorrencia";
        $tabela->Delete($id);
      endif; 
?>
<?php if(@$_GET['acao'] && $_GET['acao'] == 'update_motivo'): ?>

<?php $id = $_GET['id']; $motivo = new ocorrencias;$motivo->tabela_1 = "tipo_ocorrencia"; $dados = $motivo->Fetch($id); ?>

<form class="form-horizontal" method="post" action="">
        <div class="center">
          <div class="form-group">
            <label class="col-sm-3 control-label">Tipo de Ocorrência</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="novo_nome_motivo" placeholder="Digite um novo para <?php echo $dados->nome;?>" required="true">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-6 control-label"></label>
            <a class="btn btn-danger col-sm-2" href="?go=tipo-ocorrencia" >Voltar</a>
            <div class="col-sm-4">
              <button type="submit"class="btn btn-block btn-danger" name="update_motivo" >Editar</button>
            </div>
          </div>
        </div>
      </form>
      <br><br>

<?php else: ?>
<form class="form-horizontal" method="post" action="">
        <div class="center">
          <div class="form-group">
            <label class="col-sm-3 control-label">Novo tipo de Ocorrência</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="nome_motivo" placeholder="Digite um nome ..." required="true">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-6 control-label"></label>
            <a class="btn btn-danger col-sm-2" href="../ocorrencias/" >Voltar</a>
            <div class="col-sm-4">
              <button type="submit"class="btn btn-block btn-danger" name="insert_motivo" >Criar</button>
            </div>
          </div>
        </div>
      </form>
      <br><br>
<?php endif; ?>
      <div class="col-md-1"></div>
        <div class="table-responsive col-md-10">
        <table class="table table-striped">
          <thead class="text-center color_danger">
            <tr class="text-center">
              <th class="text-center">Valor da ocorrência</th>
              <th class="text-center">Nome da ocorrência</th>
              <th class="text-center"></th>
            </tr>
          </thead>
          <tbody class="text-center">
          <?php $tabela = new ocorrencias;$tabela->tabela_1 = "tipo_ocorrencia"; foreach($tabela->FetchAll() as $key => $value): ?>
            <tr>
             <td><?php echo $value->id; ?></td>
             <td><?php echo $value->nome; ?></td>
             <td>
                  <a href="?go=tipo-ocorrencia&acao=delete_motivo&id=<?php echo $value->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                  <a href="?go=tipo-ocorrencia&acao=update_motivo&id=<?php echo $value->id; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-wrench"></span></a>
             </td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>