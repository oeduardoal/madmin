<?php if(@$_GET['acao'] && $_GET['acao'] == 'delete_ocorrencia'):
  $deletar = new ocorrencias;
  $id = $_GET['id'];
  $deletar->DeleteOcorrencia($id);
  endif;
?>
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title text-center">No momento há <span class="badge" style="font-size: 1.6em;"><?php $count=new ocorrencias; $count->tabela_1 = "ocorrencias"; echo $count->CountTabela(); ?></span> ocorrências - <a href="?go=listar-ocorrencias" class="text-danger"><b>Ir para a Lista</b></a></h3>
  </div>
</div>

<?php
  if(@$_GET['acao'] && $_GET['acao'] == 'update_ocorrencia'):
  $id = $_GET['id'];
  $editar = new ocorrencias;
  $dados = $editar->FetchOcorrencia($id);
?>
    <form class="form-horizontal" method="post" action="">
    <div class="center">
      <div class="form-group">
        <label class="col-sm-3 control-label">Nome do Aluno</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="nome_aluno" placeholder="<?php echo $dados->nome_aluno; ?>" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Turma</label>
        <div class="col-sm-9">
          <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
          <select class="form-control" name="turma" required="true">
              <option value="<?php echo  $dados->turma; ?>"> <?php echo $dados->turma; ?>º Ano </option>
              <option value="1">1º Ano</option>
              <option value="2">2º Ano</option>
              <option value="3">3º Ano</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Curso</label>
        <div class="col-sm-9">
          <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
          <select class="form-control" name="curso" required="true">
              <option value="<?php echo  $dados->curso; ?>"> <?php echo $dados->curso; ?> </option>
              <option value="Eventos">Eventos</option>
              <option value="Enfermagem">Enfermagem</option>
              <option value="Redes">Redes</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Motivo</label>
        <div class="col-sm-9">
          <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
          <select class="form-control" name="motivo" required="true">
              <option value="<?php echo $dados->motivo; ?>"><?php echo $dados->motivo; ?></option>
            <?php $listaMotivo = new ocorrencias; $listaMotivo->tabela_1 = "tipo_ocorrencia";
              foreach ($listaMotivo->FetchAll2() as $key => $value):?>
              <option value="<?php echo $value->nome; ?>"><?php echo $value->nome; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Observação</label>
        <div class="col-sm-9">
          <textarea class="form-control" name="obs" placeholder="<?php echo $dados->obs; ?>"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
        </style>
        <button type="submit" class="btn btn-block btn-lg btn-danger" name="update_ocorrencia" >Editar Ocorrência</button>
        </div>
      </div>
  </div>
  </form>
<?php else: ?>
<?php $exibir = new ocorrencias;$exibir->tabela_1 = "ocorrencias"; $valor = $exibir->CountTabela(); if(!$valor == 0): ?>
<div class="table-responsive">
<table class="table table-bordered table-striped">
  <thead class="bg-danger">
  	<tr>
  		<th ></th>
      <th>Nome do Aluno: </th>
      <th>Motivo: </th>
  		<th>Telefone Pai: </th>
      <th>Data da ocorrência: </th>
      <th class="text-center">Ações:</th>
  	</tr>
  </thead>
  <tbody>
  <?php $tabela = new ocorrencias; $tabela->tabela_1 = "alunos"; $tabela->tabela_2 = "ocorrencias"; foreach ($tabela->FetchAll2() as $key => $value): ?>
    <tr>
      <td align="center"><img src="<?php echo "../alunos/" . $value->imagem; ?>"  width="80px" style="border-radius: 10%;" alt=""></td>
      <td><?php echo $value->nome_aluno; ?> - <b><?php echo $value->turma; ?>º Ano <?php echo $value->curso; ?></b></td>
      <td><?php echo $value->motivo; ?></td>
  		<td><a href="tel:<?php echo $value->telefonePai; ?>"><?php echo $value->telefonePai; ?></a></td>
      <td><?php echo $value->data; ?></td>
      <td class="text-center">
        <a class="btn btn-danger" href="?go=listar-ocorrencias&acao=delete_ocorrencia&id=<?php echo $value->id; ?>"><span class="glyphicon glyphicon-trash"></span></a>
        <a class="btn btn-primary" href="?go=listar-ocorrencias&acao=update_ocorrencia&id=<?php echo $value->id; ?>"><span class="glyphicon glyphicon-wrench"></span></a>
      </td>
  	</tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>
<?php endif; ?>
<?php endif; ?>
