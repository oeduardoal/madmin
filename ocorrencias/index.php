<?php
include_once "../core/login.class.php";
include_once "../core/ocorrencias.class.php";
	if(isset($_SESSION['logado'])):
?>

<!DOCTYPE html>
<html>

<head>
<style type="text/css">
.navbar{
	background-color: #d9534f !important;
}
blockquote{
	border-color: #d9534f;
}
</style>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
<script type="text/javascript" src="../../assets/js/modal.js"></script>
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
</head>
<body class="container">

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin/admin/">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class=""><a href="../admin">Painel</a></li>
            <li class=""><a href="../alunos/">Alunos</a></li>
            <li class=""><a href="../professores/">Professores</a></li>
            <li class="active"><a href="../ocorrencias/">Ocorrências</a></li>
            <li class=""><a href="../graficos/">Gerar Gráfico</a></li>
            <li class=""><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
    <?php if(@$_GET['sair'] == true):$sair = new login;$sair->Sair(); endif;?>
	    <div class="row">

	<div class="col-md-12">
	<h1 class="page-header">Ocorrências</h1>
	
	<?php
		if(isset($_POST['insert_ocorrencia'])):
			$ocorrencia = new ocorrencias;
			$ocorrencia->nome_aluno = $_POST['nome_aluno'];
			$ocorrencia->turma = $_POST['turma'];
			$ocorrencia->curso = $_POST['curso'];
			$ocorrencia->motivo = $_POST['motivo'];
			$ocorrencia->obs = $_POST['obs'];
			date_default_timezone_set('America/Sao_Paulo');
			$ocorrencia->data = date("d/m/Y") . " " . date("H:i");
			$ocorrencia->InsertOcorrencia();
		endif;
	?>
	<?php
		if(isset($_POST['update_ocorrencia'])):
			$editar = new ocorrencias;
			$id = @$_GET['id'];
			$editar->nome_aluno = $_POST['nome_aluno'];
			$editar->turma = $_POST['turma'];
			$editar->curso = $_POST['curso'];
			$editar->motivo = $_POST['motivo'];
			$editar->obs = $_POST['obs'];
			date_default_timezone_set('America/Sao_Paulo');
			$editar->data = date("d/m/Y") . " " . date("H:i");
			$editar->UpdateOcorrencia($id);
		endif;
	?>
	<?php
		if(isset($_POST['insert_motivo'])):
			$TipoMotivo = new ocorrencias;
			$TipoMotivo->nome_motivo = $_POST['nome_motivo'];
			$TipoMotivo->InsertMotivo();
		endif;
	?>

	<?php
		if(isset($_POST['update_motivo'])):
			$id = $_GET['id'];
			$editar = new ocorrencias;
			$editar->nome_motivo = $_POST['novo_nome_motivo'];
			$editar->UpdateMotivo($id);
		endif;
	?>

	<?php
		if(@$_GET['go'] == 'tipo-ocorrencia'):
			include_once "tipo-ocorrencia.php";
		elseif(@$_GET['go'] == 'listar-ocorrencias'):
			include_once "listar-ocorrencias.php";
		else:
	?>
<blockquote class="block-login" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <small>Lorem ipsum</small></blockquote>
		<form class="form-horizontal" method="post" action="">
		<div class="center">
			<div class="form-group">
				<label class="col-sm-3 control-label">Nome do Aluno</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="nome_aluno" placeholder="Ex: João da Silva Soares" required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Turma</label>
				<div class="col-sm-9">
					<!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
					<select class="form-control" name="turma" required="true">
							<option value=""> -------- </option>
							<option value="1">1º Ano</option>
							<option value="2">2º Ano</option>
							<option value="3">3º Ano</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Curso</label>
				<div class="col-sm-9">
					<!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
					<select class="form-control" name="curso" required="true">
							<option value=""> -------- </option>
							<option value="Eventos">Eventos</option>
							<option value="Enfermagem">Enfermagem</option>
							<option value="Redes">Redes</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Motivo</label>
				<div class="col-sm-9">
					<!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
					<select class="form-control" name="motivo" required="true">
							<option value=""> -------- </option>
						<?php $listaMotivo = new ocorrencias; $listaMotivo->tabela_1 = "tipo_ocorrencia"; foreach ($listaMotivo->FetchAll() as $key => $value):?>
							<option value="<?php echo $value->nome; ?>"><?php echo $value->nome; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Observação</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="obs" placeholder="OBS: (Não Necessário)"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
				</style>
				<button type="submit" class="btn btn-block btn-lg btn-danger" name="insert_ocorrencia" >Fazer Ocorrência</button>
				</div>
			</div>
	</div>
	</form>

			<form method="POST" action="" class="form-horizontal">
				<div class="center">
					<div class="form-group">
						<label class="col-sm-4"></label>
						<div class="col-sm-4" >
							<a class="btn btn-block btn-danger" href="?go=listar-ocorrencias"  style="font-size: 0.8em;">Listar Todas as ocorrêncais</a>
						</div>
						<!-- <div class="col-sm-2"></div> -->
						<div class="col-sm-4">
							<a href="?go=tipo-ocorrencia" class="btn btn-block btn-danger" name="AddTipo" style="font-size: 0.8em;">Editar Tipos de Ocorrências</a>
						</div>
					</div>
				</div>
			</form>
		<?php endif; ?>
	</div>
</div>
</div>
<?php
else:
	header("location: ../login/");
endif;
?>
