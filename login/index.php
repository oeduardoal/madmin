<?php
	require_once "../core/login.class.php";
	if(!isset($_SESSION['logado'])):
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="icon" href="../assets/css/icon.ico">

	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
</head>
<body class="container">
<!-- Login PHP -->
<?php
if(isset($_POST['btnLogin'])):
	$usuario = $_POST['usuario'];
	$senha = $_POST['senha'];

	$logar = new login;
	$logar->setUsuario($usuario);
	$logar->setSenha($senha);
	$logar->logar();
endif;
?>

<blockquote class="block-login" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <small>Lorem ipsum</small></blockquote>
<div class="row">
	<div class="col-md-12">
		<form method="POST" action="">
			<div class="jumbotron login box-shadow">
				<input type="text" name="usuario" class="form-control input" placeholder="Usuário" required autofocus/>
				<input type="password" name="senha" class="form-control input" placeholder="Senha" required autofocus/>
				<button name="btnLogin" class="btn btn-primary btn-block" onclick="return confirm('Deseja mesmo logar?');">Fazer login</button>
			</div>
		</form>
	</div>
	</div>
	<div class="row">
		<div class="col-md-12 footer">
			<br>
			<div class="col-md-5"></div>
			
			<!-- <div align="center" class="col-md-2">
				<a style="text-decoration: none;" href="?option=biblioteca"><button name="btnlogin_biblioteca" style="box-shadow: 0px 0px 10px #232625;" class="btn btn-success btn-block">Acesse a Biblioteca</button></a>
			</div> -->

			<div class="col-md-5"></div>

			<p>Desenvolvido por <a style="text-decoration: none;">Eduardo Almeida</a> - <a href="https://facebook.com/oeduardoal" target="_blank">@oeduardoal</a>.</p>
		</div>

			

   	</div>

</body>
</html>


<?php
else:
	header("location: ../admin/");
	// echo "<script>alert('Clique em sair'); history.back();</script>";
endif;
?>