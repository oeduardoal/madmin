<?php if(@$_GET['acao'] && $_GET['acao'] == 'delete_usuario'):
  $deletar = new usuarios;
  $id = $_GET['id'];
  $deletar->tabela_1 = "usuarios";
  $deletar->Delete($id);
  endif;
?>



<div class="panel" style="background: #eee !important">
  <div class="panel-heading">
    <h3 class="panel-title text-center" style="color: #222">No momento há <span class="badge" style="font-size: 1.6em;" style="color: #222"><?php $count=new usuarios; $count->tabela_1 = "usuarios"; echo $count->CountTabela(); ?></span> Usuários - <a href="?go=gerenciar-usuarios" style="color: #222"><b>Ir para a Lista</b></a></h3>
  </div>
</div>

<?php if(@$_GET['acao'] && @$_GET['acao'] == 'update_usuario'):
	$id = @$_GET['id'];
	$editar = new usuarios;
	$editar->tabela_1 = "usuarios";
	$dados = $editar->Fetch($id);
?>
	    <form class="form-horizontal" method="post" action="">
    <div class="center">
      <div class="form-group">
        <label class="col-sm-3 control-label">Nome: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="nome_usuario" placeholder="<?php echo $dados->nome_usuario; ?>" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Digite um nome de usuário: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="usuario" placeholder="<?php echo $dados->usuario; ?>" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Digite uma senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="senha" placeholder="" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Confirme a senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="senha_confirm" placeholder="" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
        <button type="submit" class="btn btn-block btn-lg btn-danger" name="update_usuario" style="background-color: black;border-color: black" >Editar usuário</button>
        </div>
      </div>
  </div>
  </form>
<?php else: ?>

<?php $exibir = new ocorrencias;$exibir->tabela_1 = "usuarios"; $valor = $exibir->CountTabela(); if(!$valor == 0): ?>

<div class="table-responsive" style="width: 50%;margin:auto">
<table class="table table-bordered table-striped">
  <thead style="background: #eee;">
  	<tr>
  		<th class="text-center" width="10%">Nome do Usuário: </th>
      	<th class="text-center" width="10%">Usuário: </th>
      	<th class="text-center" width="10%">Ações:</th>
  	</tr>
  </thead>
  <tbody>
  <?php $tabela = new usuarios; $tabela->tabela_1 = "usuarios"; foreach ($tabela->FetchAll() as $key => $value): ?>
    <tr>
      <td class="text-center"><?php echo $value->nome_usuario; ?></td>
      <td class="text-center"><?php echo $value->usuario; ?></td>
      <td class="text-center">
        <a class="btn btn-danger" href="?go=gerenciar-usuarios&acao=delete_usuario&id=<?php echo $value->id; ?>"><span class="glyphicon glyphicon-trash"></span></a>
        <a class="btn btn-primary" href="?go=gerenciar-usuarios&acao=update_usuario&id=<?php echo $value->id; ?>"><span class="glyphicon glyphicon-wrench"></span></a>
      </td>
  	</tr>
  <?php endforeach; ?>
  </tbody>
</table>
</div>
<?php endif; ?>
<?php endif; ?>