<?php
include_once "../core/login.class.php";
include_once "../core/ocorrencias.class.php";
include_once "../core/usuarios.class.php";
	if(isset($_SESSION['logado']) && $_SESSION['logado'] == "webmaster"):
?>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
  
</style>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
</head>
<body class="container">

<nav class="navbar navbar-fixed-top navbar-inverse" style="background-color: black;color: white;">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class=""><a href="../admin">Painel</a></li>
            <li class=""><a href="../alunos/">Alunos</a></li>
            <li class=""><a href="../professores/">Professores</a></li>
            <li class=""><a href="../ocorrencias/">Ocorrências</a></li>
            <li class=""><a href="../graficos/">Gerar Gráfico</a></li>
            <?php if($_SESSION['logado'] == "webmaster"): ?><li class="active"><a href="../usuarios/">Gerenciar usuários</a></li><?php endif; ?>
            <li class=""><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse "> 
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
    <?php if(@$_GET['sair'] == true):$sair = new login;$sair->Sair(); endif;?>
      <div class="row">
        <div class="col-md-12">
          <h1 class="page-header">Gerenciamento de Usuários</h1>
          
          <!-- PHP -->
          <?php
            if(isset($_POST['insert_usuario'])):
              $Insert = new usuarios;
              $Insert->nome_usuario = $_POST['nome_usuario'];
              $Insert->usuario = $_POST['usuario'];
              $Insert->senha = $_POST['senha'];
              $Insert->senha_confirm = $_POST['senha_confirm'];
              $Insert->tabela = "usuarios";
              $Insert->InsertUsuario();
            endif;
          ?>
          <?php
            if(isset($_POST['update_usuario'])):
              $editar = new usuarios;
              $id = @$_GET['id'];
              $editar->tabela = "usuarios";
              $editar->nome_usuario = $_POST['nome_usuario'];
              $editar->usuario = $_POST['usuario'];
              $editar->senha = $_POST['senha'];
              $editar->senha_confirm = $_POST['senha_confirm'];
              $editar->UpdateUsuario($id);
            endif;
          ?>



<?php if(@$_GET['go'] && @$_GET['go'] == 'gerenciar-usuarios'): ?>
  <?php include_once "gerenciar-usuarios.php"; ?>
<?php else: ?>
          <blockquote class="block-login" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <small>Lorem ipsum</small></blockquote>
    <form class="form-horizontal" method="post" action="">
    <div class="center">
      <div class="form-group">
        <label class="col-sm-3 control-label">Nome: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="nome_usuario" placeholder="Ex: João da Silva Soares" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Digite um nome de usuário: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="usuario" placeholder="Ex: CavalQuimica" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Digite uma senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="senha" placeholder="" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label">Confirme a senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="senha_confirm" placeholder="" required="true">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
        <button type="submit" class="btn btn-block btn-lg btn-danger" name="insert_usuario" style="background-color: black;border-color: black" >Criar usuário</button>
        </div>
      </div>
  </div>
  </form>
        </div>

      <form class="form-horizontal">
        <div class="center">
              <div class="form-group">
                <div class="col-sm-7"></div>
                <div class="col-sm-5">
                  <a href="?go=gerenciar-usuarios" class="btn btn-block btn-sm btn-danger" style="background-color: black;border-color: black" name="GerenciaUSer">Gerenciar usuários</a>
                </div>  
              </div>
        </div>
        </form>
      </div>
      </div>
    </div>
  <?php endif; ?>
<?php
else:
	header("location: ../login/");
endif;
?>