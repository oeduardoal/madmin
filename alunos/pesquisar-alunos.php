<?php
  if(@$_GET['acao'] && @$_GET['acao'] == 'deletar'):
    $matricula = @$_GET['matricula'];
    $tabela_1 = "alunos";
    $excluir = new alunos;
    $excluir->DeleteAluno($matricula);
  endif;
?>

<div ng-app="Tabela">
	<script src="../assets/js/angular.js"></script>
	<script src="../assets/js/Ctrl.js"></script>
<div ng-controller="Ctrl">


<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title text-center">No momento há <span class="badge" style="font-size: 1.6em;"><?php $count=new ocorrencias; $count->tabela_1 = "alunos"; echo $count->CountTabela(); ?></span> Alunos - <a href="./" class="text-success"><b>Voltar</b></a></h3>
  </div>
  <div class="panel-body">
	<input type="text" class="form-control panel-title" placeholder="Pesquise por um Aluno, Matricula ou Curso" autofocus style="" ng-model="filter"> 
  	
  </div>
</div>

<?php $exibir = new alunos ;$exibir->tabela_1 = "alunos"; $valor = $exibir->CountTabela(); if(!$valor == 0): ?>
	<div class="panel panel-default" ng-repeat="item in dados | filter: filter" ng-if="filter">
		<div class="panel-body">
			<a href="?go=pesquisar-aluno&acao=deletar&matricula={{item.matricula}}" class="btn btn-danger btn-sm excluir">Excluir</a>
			<a href="?go=pesquisar-aluno&acao=editar&matricula={{item.matricula}}" class="btn btn-primary btn-sm excluir">Editar</a>

			<a href="{{item.imagem}}" style="float:left;margin-right: 10px;margin-top: 3.5px;"><img src="{{item.imagem}}" width="80px" style="border-radius: 10%;" alt=""></a>
			
			<h5>{{item.nome_aluno}} - {{item.turma}}º Ano {{item.curso}} <a href="tel:{{item.telefonePai}}"><span class="glyphicon glyphicon-phone"></span> {{item.telefonePai}}</a></h5>
			<h5>{{item.matricula}}</h5>
		</div>
	</div>
<?php endif; ?>
</div>

</div>
</div>