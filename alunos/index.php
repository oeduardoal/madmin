<?php
include_once "../core/login.class.php";
include_once "../core/ocorrencias.class.php";
include_once "../core/alunos.class.php";
	if(isset($_SESSION['logado'])):
?>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.navbar{
	background-color: #5cb85c !important;
}
blockquote{
	border-color: #5cb85c;
}
</style>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
</head>
<body class="container">

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="../admin">Painel</a></li>
            <li  class="active"><a href="../alunos/">Alunos</a></li>
            <li><a href="../professores/">Professores</a></li>
            <li><a href="../ocorrencias/">Ocorrências</a></li>
            <li><a href="../graficos/">Gerar Gráfico</a></li>
            <li><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
    <?php if(@$_GET['sair'] == true):$sair = new login;$sair->Sair(); endif;?>
	    <div class="row">
	    <div class="col-md-12">
          <h1 class="page-header">Alunos</h1>




<?php
  if(isset($_POST['insert_aluno'])):
      $alunos = new alunos;
      $alunos->matricula = $_POST['matricula'];
      $alunos->nome_aluno = $_POST['nome_aluno'];
      $alunos->turma = $_POST['turma'];
      $alunos->curso = $_POST['curso'];
      $alunos->telefone_1 = $_POST['fone1'];
      $alunos->telefone_2 = $_POST['fone2'];

      $alunos->imagem = "img/default.jpg";
      $alunos->tabela_1 = "alunos";

      if(isset($_FILES['foto'])):
        $name = $_FILES['foto']['name'];
        $tmp_name = $_FILES['foto']['tmp_name'];
        $allowedExts = array(".jpeg", ".jpg", ".png");
    
        $ext = strtolower(substr($name,-4));
        $ext1 = strtolower(substr($_FILES['foto']['name'],-4));
        $dir = "img/";

        if(in_array($ext, $allowedExts)):
            $new_name = $_POST['matricula'] . $ext1;
            move_uploaded_file($_FILES['foto']['tmp_name'], $dir . $new_name);
            $alunos->imagem = $dir . $new_name;
        endif;
      endif;

      $alunos->InsertAluno();


  endif;
?>

<?php if(@$_GET['go'] == 'pesquisar-aluno'):
    include_once("pesquisar-alunos.php");
    else:
?>
  <blockquote class="block-login" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <small>Lorem ipsum</small></blockquote>

    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
    <div class="center">
              <div class="form-group">
            <label class="col-sm-7" for="exampleInputFile">Por favor, selecione uma foto para o Aluno:</label>
        <label class="col-sm-2"></label>
            <input class="col-sm-3" type="file" name="foto" >
          </div>
        <div class="form-group">        
          <label class="col-sm-3 control-label">Matrícula: </label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="matricula" placeholder="Ex: 51452451871" required="true">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Nome completo do aluno: </label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="nome_aluno" placeholder="Ex: João Vitor da Silva" required="true">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-3 control-label">Turma: </label>
          <div class="col-sm-9">
            <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
            <select class="form-control" name="turma" required="true">
                <option value=""> -------- </option>
                <option value="1">1º Ano</option>
                <option value="2">2º Ano</option>
                <option value="3">3º Ano</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Curso:</label>
          <div class="col-sm-9">
            <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Ex: João da Silva Soares"> -->
            <select class="form-control" name="curso" required="true">
                <option value=""> -------- </option>
                <option value="Eventos">Eventos</option>
                <option value="Enfermagem">Enfermagem</option>
                <option value="Redes">Redes</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-1"></label>
          <label class="col-sm-2 control-label">Telefones: </label>
          <label class="col-sm-1"></label>
          <div class="col-sm-4">
            <input type="text" class="form-control" name="fone1" placeholder="85 982066327">
          </div>
          <div class="col-sm-4">
            <input type="text" class="form-control" name="fone2" placeholder="85 996903117">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
              <button type="submit" class="btn btn-block btn-lg btn-success" name="insert_aluno" >Adicionar Aluno</button>
            </div>
        </div>
        <div class="form-group">
          <label class="col-sm-7"></label>
          <div class="col-sm-5">
            <a class="btn btn-block btn-success col-sm-2" href="?go=pesquisar-aluno"  style="font-size: 0.8em;">Listar Todas as ocorrêncais</a>
          </div>
        </div>
  </div>
  </form>
    <?php endif; ?>
	    </div>
    </div>
    </div>
<?php
else:
	header("location: ../login/");
endif;
?>