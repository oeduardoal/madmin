<?php
include_once "../core/login.class.php";
include_once "../core/graficos.class.php";
	if(isset($_SESSION['logado'])):
?>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.navbar{
	background-color: #f0ad4e !important;
}
blockquote{
	border-color: #f0ad4e;
}
</style>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
</head>
<body class="container">

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class=""><a href="../admin">Painel</a></li>
            <li class=""><a href="../alunos/">Alunos</a></li>
            <li class=""><a href="../professores/">Professores</a></li>
            <li class=""><a href="../ocorrencias/">Ocorrências</a></li>
            <li class="active"><a href="../graficos/">Gerar Gráfico</a></li>
            <li class=""><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
    <?php if(@$_GET['sair'] == true):$sair = new login;$sair->Sair(); endif;?>
	    <div class="row">
		    <div class="col-md-12">
	          <h1 class="page-header">Gráficos</h1>
				<blockquote class="block-login" align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <small>Lorem ipsum</small></blockquote>
			</div>
	    </div>
    </div>
<?php
else:
	header("location: ../login/");
endif;
?>