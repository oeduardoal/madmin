
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
</head>
<body class="container">

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin/app/admin/?pag=1">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="">Painel</a></li>
            <li><a href="">Alunos</a></li>
            <li><a href="">Professores</a></li>
            <li><a href="">Ocorrências</a></li>
            <li><a href="">Gerar Gráfico</a></li>
            <li><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
        <?php if(@$_GET['sair'] == true):$sair = new login;$sair->Sair(); endif;?>
      <div class="row">