<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true"); 
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
header('Access-Control-Max-Age: 86400'); 
header('Content-Type: application/json');

require_once("ocorrencias.class.php");

$json = new ocorrencias;

$json->tabela_1 = "alunos";

echo json_encode($json->FetchAll(), JSON_PRETTY_PRINT);



?>