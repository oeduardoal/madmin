<?php
  include_once "../core/login.class.php";
  include_once "../core/painel.class.php";  

	if(isset($_SESSION['logado'])):
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
    <link rel="icon" href="../assets/css/icon.ico">
	<!-- Bootsatrap -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
	<script src="../assets/js/jquery.js" type="text/javascript"></script>	
	<script src="../assets/css/bootstrap/js/bootstrap.js" type="text/javascript"></script>	

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="../assets/css/admin.css">
</head>
<body class="container">

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://localhost/madmin">Mário Alencar</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class="active"><a href="../admin">Painel</a></li>
            <li class=""><a href="../alunos/">Alunos</a></li>
            <li class=""><a href="../professores/">Professores</a></li>
            <li class=""><a href="../ocorrencias/">Ocorrências</a></li>
            <li class=""><a href="../graficos/">Gerar Gráfico</a></li>
            <?php if($_SESSION['logado'] == "webmaster"): ?><li class=""><a href="../usuarios/">Gerenciar usuários</a></li><?php endif; ?>
            <li class=""><a href="?sair=true">Sair</a></li>
          </ul>
          <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
              <li><a class=""><span class="glyphicon glyphicon-user"></span> Olá, <b><?php echo $_SESSION['logado'];  ?></b></a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
        <?php if(@$_GET['sair'] == true): $sair = new login;$sair->Sair(); endif;?>
      <div class="row">
      <script type="text/javascript" src="../../assets/js/loader.js"></script>
<div class="col-md-12">
          <h1 class="page-header">Resumo</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
          </div>
        </div>
      </div>
      
        <div class="row">
          <div class="col-md-12">
      
          <?php $tabela = new painel; if(empty($tabela->RowCountAll())): ?>
            <h2 class="sub-header">Nenhuma Ocorrência por enquanto.</h2>
          <?php else: ?>
          <h2 class="sub-header">Turmas com ocorrências</h2>

          <!-- 1 Ano -->
          <div class="table-responsive col-md-4" style="">
            <table class="table table-striped" >
              <thead class="text-center color_header">
                <tr class="text-center">
                  <th colspan="2" class="text-center">1º Ano</th>
                </tr>
              </thead>
              <tbody style="">
                <tr>
                  <td>Eventos</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(1,"Eventos"); ?></td>
                </tr>
                <tr>
                  <td>Enfermagem</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(1,"Enfermagem"); ?></td>
                </tr>
                <tr>
                  <td>Redes</td>
                  <td><?php $tabela= new painel;echo $tabela->CountTurmaCurso(1,"Redes"); ?></td>
                </tr>
              </tbody>
            </table>
          </div>

          <!-- 2 Ano -->
          <div class="table-responsive col-md-4">
            <table class="table table-striped">
              <thead class="text-center color_header">
                <tr class="text-center">
                  <th colspan="2" class="text-center">2º Ano</th>
                </tr>
              </thead>
              <tbody style="">
                <tr>
                  <td>Eventos</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(2,"Eventos"); ?></td>
                </tr>
                <tr>
                  <td>Enfermagem</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(12,"Enfermagem"); ?></td>
                </tr>
                <tr>
                  <td>Redes</td>
                  <td><?php $tabela= new painel;echo $tabela->CountTurmaCurso(12,"Redes"); ?></td>
                </tr>
              </tbody>
            </table>
          </div>  

          <!-- 1 Ano -->
          <div class="table-responsive col-md-4">
            <table class="table table-striped">
              <thead class="text-center color_header">
                <tr class="text-center">
                  <th colspan="2" class="text-center">3º Ano</th>
                </tr>
              </thead>
              <tbody style="">
                <tr>
                  <td>Eventos</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(3,"Eventos"); ?></td>
                </tr>
                <tr>
                  <td>Enfermagem</td>
                  <td><?php $tabela= new painel; echo $tabela->CountTurmaCurso(3,"Enfermagem"); ?></td>
                </tr>
                <tr>
                  <td>Redes</td>
                  <td><?php $tabela= new painel;echo $tabela->CountTurmaCurso(3,"Redes"); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        <br>
        <?php endif; ?>      
          </div>
        </div>
    </div>
    

      </div>
      </div>
<?php
else:
	header("location: ../login/");
endif;
?>