<h1 class="page-header">Editar tipos de ocorrências</h1>
<div class="table-responsive">
  <table class="table table-striped">
    <thead class="text-center color_danger">
      <tr class="text-center">
        <th class="text-center">Nome da ocorrência</th>
        <th class="text-center">Valor da ocorrência</th>
        <th class="text-center">Ação</th>
      </tr>
    </thead>
    <?php $tabela = new ocorrencias;foreach($tabela->FetchTipoMotivo() as $key => $value): ?>
    <tbody style="" class="text-center">
      <tr>
        <td><?php echo $value->nome; ?></td>
        <td><?php echo $value->valor; ?></td>
        <td>
            <a href="?pag=7&id=<?php echo $value->id; ?>" class="btn btn-danger">
              <span class="glyphicon glyphicon-trash"></span>
            </a>
            <a href="?pag=8&id=<?php echo $value->id; ?>" class="btn btn-primary">
              <span class="glyphicon glyphicon-wrench"></span>
            </a>
          </td>
      </tr>
    </tbody>
  <?php endforeach; ?>
  </table>
</div>
